<?php

/* Child theme declaration */

function iut_wp_enqueue_scripts() {

    $parenthandle = 'twentynineteen-style';
    $theme        = wp_get_theme();

    // Load parent CSS
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css', 
        array(),
        $theme->parent()->get( 'Version' )
    );

    // Load child CSS (this theme)
    wp_enqueue_style(
        'iut-style',
        get_stylesheet_uri(), 
        array( $parenthandle ),
        $theme->get( 'Version' )
    );

}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );

function iut_register_post_type_recipe() {

	register_post_type(
		'recipe',
		array(
			'labels'				=> array(
				'name'					=> 'Recettes',
				'singular_name'			=> 'Recette',
			),
			'public'				=> true,    // false = cachée de l'interface d'admin et du frontend
			'publicly_queryable'	=> true,    // Visible côté frontend ?
			'show_in_rest'			=> true,	// Nécessaire pour fonctionner avec Gutenberg
			'hierarchical'			=> false,
			'supports'				=> array( 'title', 'editor', 'thumbnail' ),
			'has_archive'			=> 'recettes',
			'rewrite'				=> array( 'slug' => 'recette' ),
		)
	);

}

add_action('init', 'iut_register_post_type_recipe', 10) ;

/**
 * Creation metabox
 */

 function iut_add_meta_boxes_recipe( $post ) {

	add_meta_box(
		'iut_mbox_recipe',                  // Unique ID
		'Infos complémentaires',            // Box title
		'iut_mbox_recipe_content', 		    // Content callback, must be of type callable
		'recipe'                          	// Post type
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recipe' );

function iut_mbox_recipe_content( $post ) {

	// Get meta value
	$iut_ingredients = get_post_meta(
		$post->ID,
		'iut-ingredients',
		true
	);

	echo '<p>';
	echo '<label for="iut-ingredients">Ingrédients</label>';
	echo '<textarea id="iut-ingredients" name="iut-ingredients" value="' . $iut_ingredients . '"></textarea>';
	echo '</p>';

}

// Save post meta
function iut_save_post( $post_id ) {

	if ( isset( $_POST['iut-ingredients'] ) && !empty( $_POST['iut-ingredients'] ) ) {

		update_post_meta(
			$post_id,
			'iut-ingredients',
			sanitize_textarea_field( $_POST['iut-ingredients'] )
		);
	}

}

add_action( 'save_post', 'iut_save_post' );